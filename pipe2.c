#include <stdio.h>
#include <unistd.h>


int main(){
	
	int pipedescriptors[2]; //pair of pipe descriptors

	int MAXBUFSIZE = 32;
	char buffer[MAXBUFSIZE+1];
	//pipe()  creates a pipe, a unidirectional data chan‐
       //nel that can be used  for  interprocess  communica‐
       //tion.
	pipe(pipedescriptors);
	// fork() This function creates  a  new  process.  The
           //   return  value  is  the zero in the child and
           //   the process-id number of the  child  in  the
           //q   parent,  or  -1  upon  error.
	if(fork() == 0){
		//child
	printf("Writing to pipe descriptor %d\n",pipedescriptors[1]);
	write(pipedescriptors[1],"test message", sizeof "test message");

	}else{
		//parent
	printf("Reading from pipe descriptor %d\n",pipedescriptors[0]);
	read(pipedescriptors[0],buffer,12);
	printf("read\" %s \" \n",buffer);


	}
	return 0;
}